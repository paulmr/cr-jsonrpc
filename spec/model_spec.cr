require "./spec_helper.cr"

# mostly tests examples from https://www.jsonrpc.org/specification

describe "json model" do
  describe "request" do
    it "rpc call with positional params" do
      input = %({"jsonrpc": "2.0", "method": "subtract", "params": [42, 23], "id": 1})
      req = Request.from_json(input)
      req.id.should eq(1)
      req.method.should eq("subtract")
      req.params.should eq([42, 23])
    end

    it "rpc call with named parameters" do
      input = %({"jsonrpc": "2.0", "method": "subtract", "params": {"subtrahend": 23, "minuend": 42}, "id": 3})
      req = Request.from_json(input)
      req.id.should eq(3)
      req.method.should eq("subtract")
      req.params.should eq({"subtrahend" => 23, "minuend" => 42})
    end

    it "a notification" do
      input = %({"jsonrpc": "2.0", "method": "update", "params": [1,2,3,4,5]})
      req = Request.from_json(input)
      req.id.should be_nil
      req.method.should eq("update")
      req.params.should eq([1, 2, 3, 4, 5])
    end

    it "invalid request" do
      input = %({"jsonrpc": "2.0", "method": 1, "params": "bar"})
      expect_raises(JSON::ParseException) do
        req = Request.from_json(input)
      end
    end
  end

  describe "response" do
    it "int result" do
      input = %({"jsonrpc": "2.0", "result": 19, "id": 1})
      req = Response(Int32).from_json(input)
      req.error.should be_nil
      req.result.should eq(19)
      req.id.should eq(1)
    end

    it "an error" do
      input = %({"jsonrpc": "2.0", "error": {"code": -32601, "message": "Method not found"}, "id": "1"})
      req = Response(Int32).from_json(input)
      req.id.should eq("1")
      req.error.should_not be_nil
      e = req.error.not_nil!
      e.code.should eq(-32601)
      e.message.should eq("Method not found")
    end
  end
end
