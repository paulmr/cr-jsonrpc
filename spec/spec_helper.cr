require "spec"
require "../src/jsonrpc.cr"

alias Request = JSONRPC::Request
alias Response = JSONRPC::Response

class MockService < JSONRPC::Service
  include JSONRPC::DefineMethods

  @responses = Channel(String).new

  @cb : Request -> Response(JSON::Any)
  def initialize(&block : Request -> Response)
    @cb = block
  end

  def initialize(&block : Request ->)
    @cb = ->(r: Request) { block.call(r) ; Response.new(JSON::Any.new(0), nil, r.id) }
  end

  def submit_request(req : String)
    @responses.send @cb.call(Request.from_json(req)).to_json
  end

  def get_next_response : String?
    @responses.receive
  end

  rpc_method(rpc_example, Int32)
  rpc_method(rpc_example_args, Int32, a : Int, b)

end

class MockServiceAuto < MockService
  include JSONRPC::AutoMethod
end
