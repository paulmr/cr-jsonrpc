require "spec"
require "./spec_helper.cr"

# just tests various, random examples that I would like to be able
# parse correctly

describe "openrpc" do
  describe "model" do
    it "parse a simple example method" do
      res = OpenRPC::Method.from_json(<<-JSON)
    {
      "name": "list_pets",
      "summary": "List all pets",
      "tags": [
        {
          "name": "pets"
        }
      ],
      "params": [
        {
          "name": "limit",
          "description": "How many items to return at one time (max 100)",
          "required": false,
          "schema": {
            "type": "integer",
            "minimum": 1
          }
        }
      ],
      "result": {
        "name": "pets",
        "description": "A paged array of pets",
        "schema": {
          "$ref": "#/components/schemas/Pets"
        }
      }
    }
JSON
      res.name.should eq("list_pets")
      res.params.size.should eq(1)
      desc = res.result.as(OpenRPC::ContentDesc)
      desc.name.should eq("pets")
      desc.schema.as(OpenRPC::Reference).ref.should eq("#/components/schemas/Pets")
    end

    it "more complicated method" do
      res = OpenRPC::Method.from_json(<<-JSON)
    {
      "name": "sleep",
      "description": " Test function.",
      "params": [
        {
          "name": "delay",
          "schema": {
            "type": "number",
            "format": "double"
          },
          "required": true
        }
      ],
      "result": {
        "name": "SleepResult",
        "schema": {
          "type": "null"
        },
        "required": true
      },
      "paramStructure": "by-position"
    }
JSON
    end
    it "schema with multiple types" do
      res = OpenRPC::Schema.from_json(<<-JSON)
      {"type": ["integer", "null"], "format": "uint32", "minimum": 0.0 }
JSON
    end
    it "schema with anyOf" do
      OpenRPC::Schema.from_json(<<-JSON)
      {
          "anyOf": [
            {
              "$ref": "#/components/schemas/ProviderInfo"
            },
            {
              "type": "null"
            }
          ]
        }
JSON
    end
  end
end
