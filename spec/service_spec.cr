require "./spec_helper.cr"

describe "service" do
  context "normal" do
    it "should call the method with no params" do
      srv = MockService.new do |req|
        req.method.should eq("foo")
        req.params.should be_nil
      end
      srv.start
      srv.call("foo")
    end

    it "should call the method with some params" do
      srv = MockService.new do |req|
        req.method.should eq("foo")
        req.id.should_not be_nil
        req.params.should eq([JSON::Any.new(10), JSON::Any.new("baz")])
      end
      srv.start
      srv.call("foo", 10, "baz")
    end

    it "second call has different id" do
      ids = [] of JSONRPC::Id
      srv = MockService.new do |req|
        ids << req.id.not_nil!
      end
      srv.start
      srv.call("foo", 10, "baz")
      srv.call("foo", 10, "baz")
      ids.size.should eq(2)
      ids[0].should_not eq(ids[1])
    end

    it "allow calling explicitly defined method no args" do
      srv = MockService.new do |req|
        req.method.should eq("rpc_example")
        req.params.should be_nil
      end
      srv.start
      srv.rpc_example
    end

    it "allow calling explicitly defined method with args" do
      srv = MockService.new do |req|
        req.method.should eq("rpc_example_args")
        req.params.should eq([JSON::Any.new(10), JSON::Any.new("foo")])
      end
      srv.start
      srv.rpc_example_args(10, "foo")
    end

  end

  context "with automethod" do
    it "call method directly with no params" do
      srv = MockServiceAuto.new do |req|
        req.method.should eq("foo")
        req.id.should_not be_nil
        req.params.should be_nil
      end
      srv.start
      srv.foo
    end
    it "call method directly with params" do
      srv = MockServiceAuto.new do |req|
        req.method.should eq("foo")
        req.id.should_not be_nil
        req.params.should eq([JSON::Any.new(10), JSON::Any.new("baz")])
      end
      srv.start
      srv.foo(10, "baz")
    end
  end
end
