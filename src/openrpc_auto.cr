require "./openrpc"

INT_TYPE = "Int32"

module OpenRPC
  struct Spec
    def resolve(maybe_ref : T | Reference) : T forall T
      return maybe_ref if maybe_ref.is_a? T
      res = case maybe_ref.ref
            when /#\/components\/contentDescriptors\/(.+)/
              components.not_nil!
                .content_descriptors.not_nil![$~[1]]
            when /#\/components\/schemas\/(.+)/
              components.not_nil!
                .schemas.not_nil![$~[1]]
            else
              raise "unknown reference type : #{maybe_ref.ref}"
            end
      # if res is not the expected type, the
      # reference is incorrect, so we do want to
      # explode
      res.as T
    end

    def describe_methods
      String.build do |s|
        methods.each do |m|
          case m
          when .is_a? OpenRPC::Reference
            raise "Method reference: #{m.ref}"
          else
            s << m.describe(self) + "\n"
          end
        end
      end
    end
  end

  struct Method
    def describe(spec)
      String.build do |sb|
        sb << name
        if !params.empty?
          sb << "("
          sb << params
               .map { |p| spec.resolve(p).describe_param(spec) }.join(",")
          sb << ")"
        end
      end
    end
  end

  struct ContentDesc
    def describe_param(spec)
      "#{name} : #{spec.resolve(schema).describe_param(spec)}"
    end
  end

  struct Schema
    def describe_param(spec)
      case type
      when "integer"
        INT_TYPE
      when "string"
        "String"
      else
        type
      end
    end
  end

end

spec = File.open(ARGV[0]? || "openrpc.json") do |fd|
  OpenRPC::Spec.from_json(fd)
end

puts spec.describe_methods
