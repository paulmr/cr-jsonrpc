module JSONRPC
  alias Id = String | Int64

  struct Request(*T)
    include JSON::Serializable
    @jsonrpc = "2.0"
    getter id, method, params

    def initialize(
      @method : String,
      @id : Id? = nil,
      @params : T? = nil
    )
    end
  end

  struct Error
    include JSON::Serializable
    getter code, message
    def initialize(@code : Int64, @message : String)
    end
  end

  # just enough info to read the response, without deserialising the
  # whole thing (for which you would need to know what type of result
  # we're expecting).
  struct BasicResponse
    include JSON::Serializable
    getter id : Id
    @[JSON::Field(ignore: true)]
    property raw : String = ""

    def as_response(result_type : T.class) : Response(T) forall T
      Response(T).from_json(raw)
    end
  end

  struct Response(T)
    include JSON::Serializable

    getter result, error, id

    @jsonrpc = "2.0"

    @error : Error?

    @[JSON::Field(emit_null: true)]
    @id : Id?

    @[JSON::Field(emit_null: true)]
    @result : T?

    def initialize(@result, @error, @id)
    end
  end
end
