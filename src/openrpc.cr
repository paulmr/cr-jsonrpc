# read an open rpc specification as JSON, convert to crystal
# JSON::Any::Type (i.e. usually results in a Hash) and recursively
# resolve any references. Then output the hash in a form that can be
# read as a literal, e.g. using something like `run()` from another
# file to generate it at compile and make it available to macros.

require "json"

class Resolver
  def initialize(@root : JSON::Any)
    raise ArgumentError.new unless @root.as_h? # if isn't a hash it doesn't need resolving
  end

  def resolve
    resolve_refs(@root)
  end

  private def resolve_ref(ref)
    keys = ref.as_s.split(/#?\//, remove_empty: true)
    keys.reduce(@root) { |obj, key| obj[key] }
  end

  private def resolve_refs(js) : JSON::Any
    case js
    when .as_h?
      if r = js["$ref"]?
        resolve_refs(resolve_ref(r))
      else
        JSON::Any.new(js.as_h.map { |k, v| { k, resolve_refs(v) } }.to_h)
      end
    when .as_a?
      JSON::Any.new(js.as_a.map { |v| resolve_refs(v) })
    else
      js # not a reference or an object that needs to be recursed, return as is
    end
  end
end

fname = ARGV[0]? || "openrpc.json"

spec = File.open(fname) do |fd|
  Resolver.new(JSON.parse(fd)).resolve
end

puts spec
