module JSONRPC
  Log = ::Log.for("jsonrpc")
  alias JS = JSON::Any::Type

  alias ResponseHandler = BasicResponse -> Bool?

  class ServerError < Exception
    getter err : Error

    def initialize(@err)
      super(@err.message)
    end
  end

  module DefineMethods
    macro rpc_method(name, ret_type, *params)
      {% if params.empty? %}
        def {{name.id}} : {{ret_type}}
          call_generic({{name.stringify}}, {{ret_type}}, {nil})
        end
      {% else %}
        def {{name.id}}({{params.splat}}) : {{ret_type}}
          call_generic({{name.stringify}},
                       {{ret_type}},
                       {{{
                          params.map do |p|
                            if p.is_a?(TypeDeclaration)
                              p.var.id
                            else
                              p.id
                            end
                          end.splat
                        }}})
        end
      {% end %}
    end
  end

  abstract class Service
    @mtx = Mutex.new
    @msg_handlers = [] of ResponseHandler
    @next_id = Atomic(Int64).new(0)
    @started = false

    private def rec_json_any(j)
      if j.is_a?(Array)
        JSON::Any.new(j.map { |v| rec_json_any(v) })
      elsif j.is_a?(Hash)
        JSON::Any.new(j.map { |k, v| {k, rec_json_any(v)} }.to_h)
      else
        JSON::Any.new(j)
      end
    end


    abstract def submit_request(req : String)
    # can return null to say the server has gone away and there won't
    # be any more messages, e.g EOF etc.
    abstract def get_next_response : String?

    def call_generic(method, result_type : T.class, params) : T forall T
      raise "RPC call before start : no response can be returned" unless @started
      ch = Channel(Response(T)).new
      id = @next_id.add(1)
      add_handler do |msg|
        if msg.id == id
          ch.send(msg.as_response(T))
          true # handled
        else
          false # not handled
        end
      end
      # now that we are ready to handle the response, we can submit the actual request
      req = Request.new(method, id, params).to_json
      Log.debug { "-> #{req}" }
      submit_request(req)
      # and now just wait for the response
      ch.receive.result.as(T) # we are expecting a result so if we didn't get one it's an error
    end

    def call(method, *params)
      call_generic(method, JSON::Any, params.to_a)
    end

    def call(method)
      call_generic(method, JSON::Any, nil)
    end

    def running
      # TODO
      true
    end

    def add_handler(&blk : ResponseHandler) : ResponseHandler
      @mtx.synchronize { @msg_handlers << blk }
      blk
    end

    def delete_handler(handler : ResponseHandler)
      @mtx.synchronize { @msg_handlers.delete(handler) }
    end

    # spawn a thread to keep listening for responses
    def start
      spawn do
        while running
          res = get_next_response
          break if res.nil?
          Log.debug { "<- #{res}" }
          basic = BasicResponse.from_json(res)
          basic.raw = res
          @mtx.synchronize do
            # call all the handlers, only keep those which return falsy
            @msg_handlers.select! { |cb| !cb.call(basic) }
          end
        end
      end
      @started = true
    end
  end

  module AutoMethod
    macro method_missing(autocall)
      call({{autocall.name.stringify}}, {{autocall.args.splat}})
    end
  end

  class ServiceProc < Service
    @ps : Process
    @write : IO
    @read : IO

    def initialize(ps)
      @write = ps.input
      @read = ps.output
      @ps = ps
    end

    def get_next_response : String?
      @read.gets
    end

    def submit_request(req)
      @write.puts(req)
    end

    def self.run_cmd(cmd)
      ps = Process.new(
        cmd, shell: true,
        input: Process::Redirect::Pipe,
        output: Process::Redirect::Pipe,
        error: Process::Redirect::Inherit)
      self.new(ps)
    end

    def shutdown
      @ps.terminate
      @ps.wait
    end
  end
end
