example taken from :

https://github.com/open-rpc/examples

and is copyrighted to them and licensed under the Apache License:

https://github.com/open-rpc/examples/blob/a5a5acc5c3f4182769a26be1ca6be48354769b4f/LICENSE.md

some extracts for these examples are also replicated in the test suite
